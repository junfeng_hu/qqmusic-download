#!/usr/bin/env python2
#!---coding:utf-8---
import os
import sys
import tempfile
import urllib2
from multiprocessing.dummy import Pool as ThreadPool

import config

WORKER_COUNT=config.WORKER_COUNT
CHUNK_SIZE=config.CHUNK_SIZE
RANGE=[]
COOKIE=config.COOKIE
agent=config.agent

class FileDown(object):
    def __init__(self,filename,url):
        req=urllib2.Request(url)
        req.headers['cookie']=COOKIE
        req.headers['user-agent']=agent
        f_headers=urllib2.urlopen(req)
        self.headers=f_headers.headers
        print self.headers.items()
        try:
            self.md5=self.headers['server-md5']
        except:
            pass

        self.size=int(self.headers['content-length'])
        f_headers.close()
        self.part_size=self.size/WORKER_COUNT
        self.range=range(0,self.size+1,self.part_size)
        self.range[-1]=self.size+1
        self.name=filename
        self.url=url
        self.partsfiles={}
        self.pool=ThreadPool(WORKER_COUNT)
        
    def start(self):
        print ("starting download...")
        self.pool.map(self.rangeDown,range(len(self.range)-1))
        self.pool.close()
        self.pool.join()
        print ("merge files...")
        self.merge()


    def rangeDown(self,index):
        write_fd,tempname=tempfile.mkstemp()
        self.partsfiles[index]=tempname
        downed=0
        total=self.range[index+1]-self.range[index]

        req=urllib2.Request(self.url)
        req.headers['Cookie']=COOKIE
        req.headers['User-Agent']=agent
        req.headers['Range']='bytes=%s-%s' %(self.range[index],self.range[index+1]-1)
        f=urllib2.urlopen(req)
        while True:
            chunk_data=f.read(CHUNK_SIZE)
            if not chunk_data:
                f.close()
                os.close(write_fd)
                break
            os.write(write_fd,chunk_data)
            downed+=len(chunk_data)
            #print("-"*int(float(downed)/total*50))
            #print ("worker %d already downloaded %.2f%%" %(index,float(downed)*100/total))
    def merge(self):
        writef=open(self.name+'.wma',"wb")
        for i in range(len(self.range)-1):
            readf=open(self.partsfiles[i],"rb")
            writef.write(readf.read())
            readf.close()
        writef.close()
        print("successfully downloaded")

    def clean(self):
        for i in range(len(self.range)-1):
            os.remove(self.partsfiles[i])




