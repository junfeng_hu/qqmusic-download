# coding: utf-8
import _winreg
uninstall=r"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
key=_winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE,uninstall)


def findInstallPath():
    i=0
    try:
        while True:
            subkey_str=_winreg.EnumKey(key,i)
            subkey=_winreg.OpenKey(key,subkey_str)
            j=0
            try:
                while True:
                    name,value,reg_type = _winreg.EnumValue(subkey,j)
                    if name in ('DisplayName','UninstallString','InstallLocation'):
                        print name, value,reg_type
                    j+=1
            except WindowsError:
                pass

            i+=1
    except WindowsError:
        return None

if __name__=="__main__":
    findInstallPath()
