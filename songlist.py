# coding: utf-8

import sqlite3
import config
import os
def get_songlist():
    conn=None
    if os.path.isfile(config.DBFILE):
        conn=sqlite3.connect(config.DBFILE)
    else:
        raise "not find db file at %s" % config.DBFILE
    cursor=conn.cursor()
    cursor.execute("SELECT wcsTitle, wcsUrl FROM SongInfo;")
    songs=cursor.fetchall()
    cursor.close()
    conn.close()
    return songs


if __name__=="__main__":
    print get_songlist()
