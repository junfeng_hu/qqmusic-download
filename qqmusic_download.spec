# -*- mode: python -*-
a = Analysis(['qqmusic_download.py'],
             pathex=['C:\\Users\\junfeng\\Desktop\\qqmusic_download'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=os.path.join('dist', 'qqmusic_download.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=True , icon='qqmusic.ico')
