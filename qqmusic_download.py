#!/usr/bin/env python2
#!---coding:utf-8---
import FileDown
import songlist
import os
import sys



if __name__=="__main__":
    ferror=open("error.txt","a+")
    songs=songlist.get_songlist()
    for index,song in enumerate(songs):
        print ("item:%d" % index)
        if os.path.isfile(song[0]+u'.wma'):
            continue
        if not song[1].startswith('http://'):
            if song[1].lower().endswith(('ogg','wmv')):
                continue
            if os.path.isfile(song[1]):
                try:
                    fr=open(song[1],"rb")
                    outname=fr.name.split('\\')[-1]
                    fw=open(outname,"wb")
                    fw.write(fr.read())
                    fr.close()
                    fw.close()
                except Exception as e:
                    print e
                    ferror.write(song[0].encode("utf-8")+'\n')
                    ferror.write(song[1].encode("utf-8")+'\n')
            else:
                ferror.write(song[0].encode("utf-8")+'\n')
                ferror.write(song[1].encode("utf-8")+'\n')
        else :
            try:
                fdown=FileDown.FileDown(*song)
                fdown.start()
                fdown.clean()
            except Exception as e:
                print e
                ferror.write(song[0].encode("utf-8")+'\n')
                ferror.write(song[1].encode("utf-8")+'\n')


    ferror.close()

